**Requirement ID: ** #10

**Requirement Type: ** Non Functional Scope of Work

**Use-Case:** Maintainability

---
**Created At:**

22.05.2018

**Author:**

Arne Wieding

**Responsible (Requirement-Engineer):**

Arne Wieding

**Category:**

- Architecture

**Weight:**

- [ ] must-have
- [x] should-have
- [ ] should-not-have
- [ ] may-have
- [ ] optional

---
**Short Description:**

The Application Code **should** be designed according to best practices and accepted industry standards.

**Description:**

Code Style guidelines **must** be followed for the language being used.
Code Test Coverage **should** be above 90%.
Functionality **should** be tested through Functional tests.
The Architecture **should** be tested through integration tests.

---
**Referenced Documents:**

- Codestyle Document Language X
- QA Guidelines

---
**Acceptance criteria:**

Reports for Test Coverage should show >= 90%
Codestyle linters and automated tests need to pass before merging new features.
Block merging otherwise.

**Accepted by:**

- CTO

---
**Priority:**

 - [ ] 1 - very low
 - [ ] 2 - low
 - [ ] 3 - medium
 - [x] 4 - high
 - [ ] 5 - very high


---
**Conflicts:**

---
**Dependencies:**

---
**legal relevance:**

- Privacy Terms
- Safety from possible Insurance claims through data breach

---
**Additional Comments:**

---
**Version:**
1.0
