**Requirement ID: ** #1

**Requirement Type: ** Functional Scope of Work

**Use-Case:** Setup

---
**Created At:**

22.05.2018

**Author:**

Arne Wieding

**Responsible (Requirement-Engineer):**

Arne Wieding

**Category:**

- Hardware, User Profile

**Weight:**

- [x] must
- [ ] should
- [ ] should-not
- [ ] may
- [ ] optional

---
**Short Description:**

The User **must** able to connect the Dongle and AR Display to his Smartphone

**Description:**

The User **should** place the Display and Dongle based on instructions shown in the Smartphone App.

After turning on the Display the Dongle has to be long-pressed to be discoverable by the Display via Bluetooth. The Display **must** show the found Dongle Adapter and offer to connect

Afterwards the Smartphone App **must** connect to the Display via Bluetooth and confirm the connection of all components.

---
**Referenced Documents:**

- Hardware-Setup-Instructions.pdf

- UML Data Flow Activity Diagram

---
**Acceptance criteria:**

The Smartphone **must** be connected to both devices and communicate data.


**Accepted by:**

- Arne Wieding
- Product Manager/Owner

---
**Priority:**


 - [ ] 1 - very low
 - [ ] 2 - low
 - [ ] 3 - medium
 - [ ] 4 - high
 - [x] 5 - very high


---
**Conflicts:**

---
**Dependencies:**

- Bluetooth Connectivity
- Instruction Manual

---
**legal relevance:**

- None

---
**Additional Comments:**


---
**Version:**
1.0
