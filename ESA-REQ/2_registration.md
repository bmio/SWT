**Requirement ID: ** #2

**Requirement Type: ** Functional Scope of Work

**Use-Case:** User Profile

---
**Created At:**

22.05.2018

**Author:**

Arne Wieding

**Responsible (Requirement-Engineer):**

Arne Wieding

**Category:**

- User Profile

**Weight:**

- [x] must-have
- [ ] should-have
- [ ] should-not-have
- [ ] may-have
- [ ] optional

---
**Short Description:**
The User **must** be able to register an Account in the System.

**Description:**
Registering an Account requires the following Data:
- Firstname
- Lastname
- E-Mail
- Password

After registering, the User has to validate the confirmation E-Mail.

---
**Referenced Documents:**
- User Database Schema

---
**Acceptance criteria:**

Successful registration including E-Mail validation


**Accepted by:**

- Arne Wieding
- Product Manager/Owner

---
**Priority:**


 - [ ] 1 - very low
 - [ ] 2 - low
 - [ ] 3 - medium
 - [ ] 4 - high
 - [x] 5 - very high


---
**Conflicts:**

---
**Dependencies:**
- #1 Setup

---
**legal relevance:**

- Terms of Service Acceptance
- Privacy Agreement Acceptance

---
**Additional Comments:**


---
**Version:**
1.0
