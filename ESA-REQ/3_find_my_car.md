**Requirement ID: ** #3

**Requirement Type: ** Functional Scope of Work

**Use-Case:** Find My Car

---
**Created At:**

22.05.2018

**Author:**

Arne Wieding

**Responsible (Requirement-Engineer):**

Arne Wieding

**Category:**

- Mobile App

**Weight:**

- [ ] must-have
- [X] should-have
- [ ] should-not-have
- [ ] may-have
- [ ] optional

---
**Short Description:**
The User **should** be able to find her car through the app.

**Description:**
The Mobile app constantly updates the GPS position of the car in the backend.

When the User is out of the car, she can always find the last location of the car in the app.

---
**Referenced Documents:**
- UML Usecase Diagram
- App UI Layout Sketch

---
**Acceptance criteria:**

After leaving the car, the App always shows the correct last position of the car based on GPS coordinates.


**Accepted by:**

- Arne Wieding
- Product Manager/Owner

---
**Priority:**

 - [ ] 1 - very low
 - [ ] 2 - low
 - [ ] 3 - medium
 - [x] 4 - high
 - [ ] 5 - very high


---
**Conflicts:**

---
**Dependencies:**
- #2 Registration
- #11 Security

---
**legal relevance:**

- Insurance (Theft protection)

---
**Additional Comments:**

---
**Version:**
1.0
