**Requirement ID: ** #4

**Requirement Type: ** Functional Scope of Work

**Use-Case:** Logbook

---
**Created At:**

22.05.2018

**Author:**

Arne Wieding

**Responsible (Requirement-Engineer):**

Arne Wieding

**Category:**

- App

**Weight:**

- [ ] must-have
- [X] should-have
- [ ] should-not-have
- [ ] may-have
- [ ] optional

---
**Short Description:**
The User **should** be able to record the current route

**Description:**
If the Logbook functionality is activated it should automatically record the current route information for later retrieval.

---
**Referenced Documents:**
- UML Usecase Diagram
- App UI Layout Sketch

---
**Acceptance criteria:**

After stopping the car, the last route should be correctly saved in the system.

**Accepted by:**

- Arne Wieding
- Product Manager/Owner

---
**Priority:**

 - [ ] 1 - very low
 - [ ] 2 - low
 - [ ] 3 - medium
 - [x] 4 - high
 - [ ] 5 - very high


---
**Conflicts:**

---
**Dependencies:**
- #2 Registration
- #11 Security

---
**legal relevance:**

- Adhere to legal standards for Fahrtenbuch to be used
for tax returns

---
**Additional Comments:**

---
**Version:**
1.0
