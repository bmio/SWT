**Requirement ID: ** #5

**Requirement Type: ** Functional Scope of Work

**Use-Case:** Logbook

---
**Created At:**

22.05.2018

**Author:**

Arne Wieding

**Responsible (Requirement-Engineer):**

Arne Wieding

**Category:**

- App

**Weight:**

- [ ] must-have
- [X] should-have
- [ ] should-not-have
- [ ] may-have
- [ ] optional

---
**Short Description:**
The User **should** be able to see a list of recorded routes

**Description:**
In the mobile App, the Logbook section should show a list of all recorded routes which can be filtered by date and includes pagination

---
**Referenced Documents:**
- App UI Layout Sketch

---
**Acceptance criteria:**

The Logbook results are shown paginated and different pages can be accessed. The result can also be filtered by date and pagination should update accordingly.

**Accepted by:**

- Arne Wieding
- Product Manager/Owner

---
**Priority:**

 - [ ] 1 - very low
 - [ ] 2 - low
 - [ ] 3 - medium
 - [x] 4 - high
 - [ ] 5 - very high


---
**Conflicts:**

---
**Dependencies:**
- Registration

---
**legal relevance:**

- Adhere to legal standards for Fahrtenbuch to be used
for tax returns

---
**Additional Comments:**

---
**Version:**
1.0
