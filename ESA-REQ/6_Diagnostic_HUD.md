**Requirement ID: ** #6

**Requirement Type: ** Functional Scope of Work

**Use-Case:** Diagnostics

---
**Created At:**

22.05.2018

**Author:**

Arne Wieding

**Responsible (Requirement-Engineer):**

Arne Wieding

**Category:**

- HUD

**Weight:**

- [x] must-have
- [ ] should-have
- [ ] should-not-have
- [ ] may-have
- [ ] optional

---
**Short Description:**

The HUD AR Display **must** show current diagnostics data.

**Description:**

The ODBC dongle sends the following diagnostics to the mobile App:

- Speed
- Fuel consumption
- Current Gear
- Current RPM
- Fluid temperatures

This information **must** be shown on the AR HUD in realtime.

---
**Referenced Documents:**
- HUD UI Layout Sketch

---
**Acceptance criteria:**

Live Diagnostics data is streamed in realtime to the HUD Display after the car has been started.

**Accepted by:**

- Arne Wieding
- Product Manager/Owner

---
**Priority:**

 - [ ] 1 - very low
 - [ ] 2 - low
 - [ ] 3 - medium
 - [ ] 4 - high
 - [x] 5 - very high


---
**Conflicts:**

---
**Dependencies:**
- #2 Registration

---
**legal relevance:**

---
**Additional Comments:**

---
**Version:**
1.0
