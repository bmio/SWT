**Requirement ID: ** #7

**Requirement Type: ** Functional Scope of Work

**Use-Case:** Points of Interest

---
**Created At:**

22.05.2018

**Author:**

Arne Wieding

**Responsible (Requirement-Engineer):**

Arne Wieding

**Category:**

- HUD

**Weight:**

- [x] must-have
- [ ] should-have
- [ ] should-not-have
- [ ] may-have
- [ ] optional

---
**Short Description:**

The HUD AR Display **must** show Points of Interest in the vicinity of the cars current location.

**Description:**

Based on the current Car Location, the Google Maps API needs to queried to get current Points of Interests.

Points of Interest should be filterable from the Mobile App.

This information **must** be shown on the AR HUD in realtime.

---
**Referenced Documents:**
- HUD UI Layout Sketch

---
**Acceptance criteria:**

Points of Interest matching the current location are shown on the HUD display and are constantly updated.

If the list of POIs is filtered (eg. gas stations) only the filtered POIs should show up.

All POIs should show current distance and Rating.

**Accepted by:**

- Arne Wieding
- Product Manager/Owner

---
**Priority:**

 - [ ] 1 - very low
 - [ ] 2 - low
 - [ ] 3 - medium
 - [ ] 4 - high
 - [x] 5 - very high


---
**Conflicts:**

---
**Dependencies:**
- #2 Registration

---
**legal relevance:**

---
**Additional Comments:**

---
**Version:**
1.0
