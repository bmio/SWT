**Requirement ID: ** #8

**Requirement Type: ** Functional Scope of Work

**Use-Case:** Points of Interest

---
**Created At:**

22.05.2018

**Author:**

Arne Wieding

**Responsible (Requirement-Engineer):**

Arne Wieding

**Category:**

- Routing
- HUD

**Weight:**

- [ ] must-have
- [x] should-have
- [ ] should-not-have
- [ ] may-have
- [ ] optional

---
**Short Description:**

The User **must** be able to select a specific POI to show routing information.

**Description:**

After the User selected a POI, live routing/navigation information will be shown on the HUD display to guide the User to the destination.

---
**Referenced Documents:**

- HUD UI Layout Sketch

---
**Acceptance criteria:**

Navigation information to the currently selected POI are shown on the HUD and updated in realtime until the destination is reached.

**Accepted by:**

- Arne Wieding
- Product Manager/Owner

---
**Priority:**

 - [ ] 1 - very low
 - [ ] 2 - low
 - [ ] 2 - low
 - [ ] 3 - medium
 - [x] 4 - high
 - [ ] 5 - very high


---
**Conflicts:**

---
**Dependencies:**
- #7 POIs HUD

---
**legal relevance:**

---
**Additional Comments:**

---
**Version:**
1.0
