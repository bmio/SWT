**Requirement ID: ** #9

**Requirement Type: ** Non Functional Scope of Work

**Use-Case:** Security

---
**Created At:**

22.05.2018

**Author:**

Arne Wieding

**Responsible (Requirement-Engineer):**

Arne Wieding

**Category:**

- Architecture

**Weight:**

- [x] must-have
- [ ] should-have
- [ ] should-not-have
- [ ] may-have
- [ ] optional

---
**Short Description:**

The Application **must** be designed in a secure way.

**Description:**

The system architecture **must** be secure in the sense of not exposing critical information and in general only exposing necessary information.

Individual User Permission checks are required and Data collection must be in accordance with local data privacy laws.

---
**Referenced Documents:**

- Security Guidelines

---
**Acceptance criteria:**

The system design must be reviewed and approved by the CTO and an independent Security Consultant

**Accepted by:**

- CTO

---
**Priority:**

 - [ ] 1 - very low
 - [ ] 2 - low
 - [ ] 3 - medium
 - [x] 4 - high
 - [ ] 5 - very high


---
**Conflicts:**

---
**Dependencies:**

---
**legal relevance:**

- Privacy Terms
- Safety from possible Insurance claims through data breach

---
**Additional Comments:**

---
**Version:**
1.0
